import configparser
import database


class Classifiers:
    def __init__(self):
        self.category_dict = {}
        self.from_config()

    def from_config(self):
        config = configparser.ConfigParser(allow_no_value=True)
        config.optionxform = str
        config.read('categories.ini')
        for category in config.sections():
            for classifier in config[category]:                
                self.category_dict[classifier] = category

    def to_database(self):
        db = database.Database('transactions.sqlite')
        categories_table = database.CategoriesTable(db)
        categories_table.clear_tab()
        for subject in self.category_dict:
            category = self.category_dict[subject]
            categories_table.add_row(subject_substring=subject, category=category)
        categories_table.commit()


if __name__ == "__main__":
    classifiers = Classifiers()
    classifiers.to_database()
