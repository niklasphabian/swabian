PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "categories" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`subject_substring`	TEXT,
	`category`	TEXT
);

CREATE TABLE "transactions" (
"index" INTEGER,
  "date" TIMESTAMP,
  "amount" REAL,
  "subject" TEXT,
  "balance" REAL,
  "category" TEXT
);

DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('categories',110);
CREATE INDEX "ix_transactions_index"ON "transactions" ("index");
COMMIT;
