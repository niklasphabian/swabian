import sqlite3


class Database:
    def __init__(self, db_path):
        self.connection = sqlite3.connect(db_path)
        self.cursor = self.connection.cursor()

    def commit(self):
        self.connection.commit()

    def close(self):
        self.connection.close()

    def clear_tab(self, table_name):
        query = 'DELETE FROM {}'.format(table_name)
        self.cursor.execute(query)
        self.commit()


class DBTable:
    def __init__(self, database):
        self.database = database

    def clear_tab(self):
        query = 'DELETE FROM {}'.format(self.table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def commit(self):
        self.database.commit()


class TransactionTable(DBTable):
    def add_row(self, row):
        pass

    def delete_row(self):
        pass


class CategoriesTable(DBTable):
    table_name = 'categories'

    def add_row(self, subject_substring, category):
        query = 'INSERT INTO {table_name} (subject_substring, category) VALUES (?,?)'
        query = query.format(table_name=self.table_name)
        self.database.cursor.execute(query, (subject_substring, category))

