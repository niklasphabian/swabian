from database import Database
from query import Query
import matplotlib.pyplot as plt
import pandas
from plots import Plot

db = Database('transactions.sqlite')
colors = ['#543005', '#8c510a', '#bf812d', '#dfc27d', '#f6e8c3', '#c7eae5', '#80cdc1', '#35978f', '#01665e', '#003c30']


def per_category():
    query = Query('sql/per_category_total.sql')
    query.format(limit=9)
    ret = query.execute_fetchall(db)
    labels = []
    sizes = []
    for row in ret:
        label = row[0].replace('_', '\_')
        labels.append(label)
        sizes.append(-row[1])
    plt.pie(sizes, colors=colors, labels=labels, shadow=True, startangle=90)
    plt.axis('equal')
    plt.savefig('figures/per_cat.png')


def get_timeline(category):
    query = Query('sql/category_over_month.sql')
    query.format(category=category)
    ret = query.execute_fetchall(db)
    df = pandas.DataFrame.from_records(ret, columns=['date', category], index='date')    
    return df

def plot_timelines():
    categories = ['costco', 'bar', 'restaurant', 'amazon', 'gas']
    timeline_plot = Plot()        
    for category in categories[0:]:                        
        df = get_timeline(category)[category]        
        df.fillna(value=0, inplace=True)
        timeline_plot.plot_pandas(df)            
    timeline_plot.y_label('Money spent \n in USD')    
    timeline_plot.x_label('')    
    timeline_plot.rotate_xticks(90)
    timeline_plot.save_fig('figures/timeline.png')


if __name__ == '__main__':
    per_category()
    plot_timelines()
