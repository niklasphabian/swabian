SELECT
	categories.category,
	sum(amount)
FROM
	transactions,
	categories
WHERE instr(transactions.subject, categories.subject_substring)
GROUP BY categories.category
ORDER BY sum(amount)
LIMIT {limit}