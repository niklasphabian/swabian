import pandas
import database
import classifiers
import copy
import glob

classifier = classifiers.Classifiers()
classifier.to_database()

column_names = ['date', 'amount', 'pass', 'pass2', 'subject']

filename_checking = sorted(glob.glob('data/*Checking*'))[-1]
filename_credit = sorted(glob.glob('data/*Credit*'))[-1]


db = database.Database('transactions.sqlite')
table_name = 'transactions'


def load_file(filename):
    # Read file to dataframe
    df = pandas.read_csv(filename, names=column_names, parse_dates=['date'])
    df.drop(['pass', 'pass2'], axis=1, inplace=True)
    df = df.reindex(index=df.index[::-1])

    # Add a total balance column
    df['balance'] = df['amount'].cumsum()

    # Add a category column
    df['category'] = pandas.Series('', index=df.index)

    # Classify
    for index, row in df.iterrows():
        for subject in classifier.category_dict:
            if subject.lower() in row['subject'].lower():
                category = classifier.category_dict[subject]
                df.loc[index, 'category'] = category

    # extract cashback
    for index, row in df.iterrows():
        if 'PURCHASE WITH CASH BACK' in row['subject']:
            cash_back = copy.deepcopy(row)
            purchase = copy.deepcopy(row)
            cash_back['amount'] = -float(cash_back['subject'].split('$ ')[1].split(' ')[0])
            cash_back['subject'] = cash_back['subject'].split('PURCHASE WITH ')[1]
            cash_back['category'] = 'cash'
            purchase['amount'] = purchase['amount'] - cash_back['amount']
            purchase['subject'] = 'PURCHASE ' + purchase['subject'].split('$ ')[1].split(' ', 1)[1]
            df = df.drop(index)
            df = df.append(cash_back)
            df = df.append(purchase)
    # Upload to db
    df.to_sql(name=table_name, con=db.connection, if_exists='append')


if __name__ == '__main__':
    db.clear_tab(table_name=table_name)
    load_file(filename_checking)
    load_file(filename_credit)
